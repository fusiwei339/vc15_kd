d3.mdsView = function() {
    var width, height;

    function mdsView(g, data) {

        var conf = Template.mainview.configure.mdsView;
        // var col=conf.col, row=conf.row;

        _.each(data, function(d) {
            d.coord = JSON.parse(d.coord)
        })
        var dataToUse = data.slice(0);
        var mdsFilter = Session.get('mdsFilterVisitors');
        if (mdsFilter) {
            dataToUse = dataToUse.filter(function(d) {
                return mdsFilter.indexOf(d.category) == -1;
            })
        }
        var dataObj = {}
        data.forEach(function(d) {
            dataObj['' + d.id] = d;
        })

        var xDomain, yDomain, colorDomain;
        colorDomain = d3.extent(data, function(d) {
            return d.nFromConn;
        })
        xDomain = d3.extent(data, function(d) {
            return d.coord[0];
        })

        yDomain = d3.extent(data, function(d) {
            return d.coord[1];
        })

        var ids = _.map(dataToUse, function(d) {
            return d.id;
        })

        var colorScale = d3.scale.linear()
            .domain(colorDomain)
            .range(['#edf8b1', '#2c7fb8']);

        var xScale = d3.scale.linear()
            .domain(xDomain)
            .range([conf.padding, width - conf.padding]),

            yScale = d3.scale.linear()
            .domain(yDomain)
            .range([conf.padding, height - conf.padding]);


        g.selectAll('circle')
            .data(dataToUse)
            .enter()
            .append('circle')
            .attr('class', 'mdsView')
            .attr('id', function(d) {
                return 'n' + d.id;
            })
            .attr('cx', function(d) {
                return xScale(d.coord[0]);
            })
            .attr('cy', function(d) {
                return yScale(d.coord[1]);
            })
            .attr('r', function(d) {
                return 3;
            })
            .attr('fill', function(d) {
                return colorScale(d.nFromConn);
            })
            .on('click', function(d) {
                Session.set('getId', d.id);

            })
            .append('svg:title')
            .text(function(d) {
                return d.id;
            })
            .attr('fill', '#74add1')
            .attr('opacity', 0.5)


        //lines
        var date = Session.get('date');
        var id=Session.get('getId');

        var connections = []
        var subscribe_handler = Meteor.subscribe('mdsFromConn', id, date);
        if (subscribe_handler.ready()) {
            connections = Collection.mdsFromConn.find({
                id: id,
                day: date
            }).fetch()[0].fromArr;
        }

        if (! _.isEmpty(connections)) {
            console.log(connections)

            g.selectAll('.edge')
                .data(connections)
                .enter()
                .append('path')
                .attr('class', "edge")
                .attr("fill", "none")
                .attr("stroke-width", function(d) {
                    return 2;
                })
                .attr("stroke", function(d) {
                    return '#CCC';
                })
                .attr('d', function(d) {
                    var to=dataObj[''+id];
                    var from=dataObj[''+d];
                    var fromx, fromy, tox, toy;
                    if(! (to && from)){
                        console.log(from)
                    }else{
                        fromx=from.coord[0];
                        fromy=from.coord[1];
                        tox=to.coord[0];
                        toy=to.coord[1];

                        var r = (xScale(tox) - xScale(fromx)) * (xScale(tox) - xScale(fromy));
                        r = r + (yScale(toy) - yScale(fromy)) * (yScale(toy) - yScale(fromy));
                        r = Math.sqrt(r) * 2;
                        if(! r){
                            console.log(from)
                        }else{
                            var path = "M" + xScale(fromx) + "," + yScale(fromy);
                            path = path + "A" + r + "," + r + " 0 0 1";
                            path = path + xScale(tox) + "," + yScale(toy);
                            return path;

                        }

                    }
                })

        }

        var highlightNode = +Session.get('highlightNode');
        if (highlightNode) {
            if (ids.indexOf(highlightNode) == -1) {
                Session.set('highlightMsg', highlightNode + ' is not in the view')
            } else {
                $('#n' + highlightNode).attr('fill', 'red')
                $('#n' + highlightNode).attr('r', '6')
            }

        }

    }


    mdsView.width = function(x) {
        var res;
        if (!arguments.length) {
            res = width;
        } else {
            width = x;
            res = mdsView;
        }
        return res;
    };

    mdsView.height = function(x) {
        var res;
        if (!arguments.length) {
            res = height;
        } else {
            height = x;
            res = mdsView;
        }
        return res;
    };


    return mdsView;
}
