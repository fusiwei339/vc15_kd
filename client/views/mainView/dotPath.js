d3.dotPath = function() {
    var width, height;

    function dotPath(g, data) {

        var conf = Template.mainview.configure.img;
        var col = conf.col,
            row = conf.row;

        var simplifiedData = getPathFreq(data);
        var colorScale = d3.scale.linear().
        domain(d3.extent(simplifiedData, function(d) {
                return d.freq;
            }))
            .range(['#c6dbef', '#084594']);

        g.selectAll('rect')
            .data(simplifiedData)
            .enter()
            .append('rect')
            .attr('class', function(d) {
                if (d.type === 'check-in') {
                    return 'dotPath_checkin';
                } else {
                    return 'dotPath_move'
                }
            })
            .attr('x', function(d) {
                return d.X * width / col;
            })
            .attr('y', function(d) {
                return (row - d.Y - 1) * height / row;
            })
            .attr('width', function(d) {
                return width / col;
            })
            .attr('height', function(d) {
                return height / row;
            })
            .attr('fill', function(d) {
                if (d.type !== 'check-in') {
                    return colorScale(d.freq);
                } else {
                    //defined in CSS file;
                }
            })
            .attr('opacity', 0.9)


        //draw overlay
        g.append('rect')
            .attr('x', function(d) {
                return 0;
            })
            .attr('y', function(d) {
                return 0;
            })
            .attr('width', function(d) {
                return width;
            })
            .attr('height', function(d) {
                return height;
            })
            .attr('fill', 'white')
            .attr('opacity', '0')
            .on('mousemove', function(e){
                var coord=d3.mouse(this);
                Session.set('mouseTrack', coord);
            })



    }

    var getPathFreq = function(data) {
        var obj = {};
        for (var i = 0; i < data.length; i++) {
            var step = data[i];
            if (obj.hasOwnProperty(step.X + ',' + step.Y)) {
                if (obj[step.X + ',' + step.Y].type === 'check-in') {
                    obj[step.X + ',' + step.Y].freq++;
                } else {
                    obj[step.X + ',' + step.Y].freq++;
                    obj[step.X + ',' + step.Y].type = step.type;
                }

            } else {
                obj[step.X + ',' + step.Y] = {
                    freq: 1,
                    type: step.type,
                }
            }
        }
        var ret = [];
        for (var key in obj) {
            var temp = obj[key];
            ret.push({
                type: temp.type,
                X: key.split(',')[0],
                Y: key.split(',')[1],
                freq: temp.freq,
            })
        }
        return ret;
    }


    dotPath.width = function(x) {
        var res;
        if (!arguments.length) {
            res = width;
        } else {
            width = x;
            res = dotPath;
        }
        return res;
    };

    dotPath.height = function(x) {
        var res;
        if (!arguments.length) {
            res = height;
        } else {
            height = x;
            res = dotPath;
        }
        return res;
    };


    return dotPath;
}
