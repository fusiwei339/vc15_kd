Template.mainview.rendered = function() {
    $('.selectpicker').selectpicker();

    Deps.autorun(function() {

        //get path data
        var singlePath = [];
        var dateConst = ['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']

        var subscribe_handler = Meteor.subscribe('singlePath', Session.get('getId'));
        if (subscribe_handler.ready()) {
            singlePath = Collection.mc1.find({
                id: Session.get('getId'),
                // 'Timestamp':{$gt:dateConst[date], $lt:dateConst[date+1]}
            }).fetch();
        }

        if (!_.isEmpty(singlePath)) {

            //draw path

            for (var i = 1; i < 4; i++) {
                var imgConf = Template.mainview.configure.img;
                imgConf.width = $('#picContainer' + i).width();
                imgConf.height = $('#picContainer' + i).width();

                var this_day_path = Collection.mc1.find({
                    id: Session.get('getId'),
                    'Timestamp': {
                        $gt: dateConst[i - 1],
                        $lt: dateConst[i]
                    }
                }).fetch();

                d3.select("#picCanvas" + i)
                    .attr("width", imgConf.width)
                    .attr("height", imgConf.height);
                $("#picCanvas" + i).empty();

                d3.select('#picCanvas' + i)
                    .append('image')
                    .attr('x', 0)
                    .attr('y', 0)
                    .attr('width', imgConf.width)
                    .attr('height', imgConf.height)
                    .attr('xlink:href', './map.jpg')

                var picCanvasG = d3.select("#picCanvas" + i).append('g')
                    .attr("class", "picCanvasG" + i)

                //setup time position canvas
                d3.select("#timePoint" + i)
                    .attr("width", imgConf.width)
                    .attr("height", 30);
                $("#timePoint" + i).empty();

                var timePointG = d3.select("#timePoint" + i).append('g')
                    .attr("class", "timePointG" + i)
                    //draw alpha rect
                picCanvasG.append('rect')
                    .attr('x', 0)
                    .attr('y', 0)
                    .attr('width', imgConf.width)
                    .attr('height', imgConf.height)
                    .attr('fill', 'white')
                    .attr('opacity', 0.9)

                if (_.isEmpty(this_day_path)) {
                    continue;
                }

                // drawDashedLines(picCanvasG);
                drawPath(picCanvasG, this_day_path);
                drawTimePoint(timePointG, this_day_path);

            }

        }


    })

    Deps.autorun(function() {
        var imgConf = Template.mainview.configure.img;
        var selectedTime = Session.get('selectedTime');
        if (selectedTime) {
            var heatmap_subscribe_handler = Meteor.subscribe('timePoint', selectedTime);
            var heatmapData;

            var sec = str2sec(selectedTime);
            var day = selectedTime.split(' ')[0];
            var start = sec - 15,
                end = sec + 15;

            var istart = day + ' ' + secondsToTime(start);
            var iend = day + ' ' + secondsToTime(end);

            if (heatmap_subscribe_handler.ready()) {
                heatmapData = Collection.mc1.find({
                    'Timestamp': {
                        $gt: istart,
                        $lt: iend
                    }
                }).fetch();
            }
            if(! _.isEmpty(heatmapData)){
                _.uniq(heatmapData, false, function(d){
                    return d.id;
                })
                // //heatmap canvas
                d3.select("#simpleHeatmap")
                    .attr("width", imgConf.width)
                    .attr("height", imgConf.height);

                Session.set('heatmapData', heatmapData);
                drawHeatmap(heatmapData)
            }
        }
    })


    Deps.autorun(function(){

        var coord=Session.get('mouseTrack')
        if(coord){
            for(var i=1;i<4;i++){
                d3.select("#mouseTrackG"+i).remove();
                var mouseTrackG= d3.select("#picCanvas" + i).append('g')
                    .attr("id", "mouseTrackG" + i)
                mouseTrackG.append('circle')
                    .attr('cx', coord[0])
                    .attr('cy', coord[1])
                    .attr('r', 15)
                    .attr('fill', 'none')
                    .attr('stroke', '#aaa')
                    .attr('stroke-width', '2px')
            }
            if(Session.get('heatmapData')){
                $('#picCanvas4').empty()
                var canvasMouseTrackG= d3.select("#picCanvas4").append('g')
                    .attr("id", "mouseTrackG" + 4)
                canvasMouseTrackG.append('circle')
                    .attr('cx', coord[0])
                    .attr('cy', coord[1])
                    .attr('r', 15)
                    .attr('fill', 'none')
                    .attr('stroke', '#aaa')
                    .attr('stroke-width', '2px')
            }
        }

        var mouseTS = Session.get('mouseTS');
        if (mouseTS) {

            var date=mouseTS.split(' ')[0];
            var mouseTSG=undefined;
            switch (date){
                case '2014-6-06': 
                    mouseTSG=d3.select("#mouseTrackG"+1);
                    break;
                case '2014-6-07':
                    mouseTSG=d3.select("#mouseTrackG"+2);
                    break;
                case '2014-6-08':
                    mouseTSG=d3.select("#mouseTrackG"+3);
                    break;
            }

            var doc = Collection.mc1.findOne({
                id: Session.get('getId'),
                'Timestamp': {
                    $gt: mouseTS
                }
            });

            var conf = Template.mainview.configure.img;
            var width=conf.width, height=conf.height, col=conf.col, row=conf.row;

            if(doc){

                mouseTSG.append('rect')
                    .attr('x', function() {
                        return doc.X * width / col;
                    })
                    .attr('y', function() {
                        return (row - doc.Y - 1) * height / row;
                    })
                    .attr('width', function(d) {
                        return 10;
                    })
                    .attr('height', function(d) {
                        return 10;
                    })
                    .attr('fill', 'black')

            }
        }
    })

    Deps.autorun(function() {
        //get mds points
        var date = Session.get('date');
        Meteor.subscribe('mds', date)
        var mdsData = Collection.mds.find({
            day: date
        }).fetch();

        //draw mds
        var mdsConf = Template.mainview.configure.mdsView;
        mdsConf.width = $('#mdsContainer').width();
        mdsConf.height = $('#mdsContainer').width();

        d3.select("#mdsCanvas")
            .attr("width", mdsConf.width)
            .attr("height", mdsConf.height);
        $("#mdsCanvas").empty();

        var mdsCanvasG = d3.select("#mdsCanvas").append('g')
            .attr("class", "mdsCanvasG")
            .attr('transform', geom.transform.begin().translate(0, 0).end())

        drawMDS(mdsCanvasG, mdsData);
//        drawDashedLines(mdsCanvasG)

    })

}

var drawTimePoint = function(g, data) {

    var conf = Template.mainview.configure.img;
    var timePointView = d3.timePointView()
        .width(conf.width)
        .height(10);

    timePointView(g, data)
}

var drawHeatmap = function(data) {
    var canvas = document.getElementById('simpleHeatmap');
    var heatmap = createWebGLHeatmap({
        canvas: canvas,
        intensityToAlpha: true
    });
    var conf = Template.mainview.configure.img;
    var col = conf.col,
        row = conf.row,
        width = conf.width,
        height = conf.height;

    for (var i = 0; i < data.length; i++) {
        var d = data[i];
        heatmap.addPoint(d.X * width / col, (row - d.Y - 1) * height / row, 7, 0.5)
    }

    heatmap.update();
    heatmap.display();
}
var drawMDS = function(g, data) {

    var mdsConf = Template.mainview.configure.mdsView;
    var mdsView = d3.mdsView()
        .width(mdsConf.width)
        .height(mdsConf.height);

    mdsView(g, data)
}

var drawDashedLines = function(g) {

    var row = 100,
        col = 100;
    var conf = Template.mainview.configure;
    var width = conf.img.width,
        height = conf.img.height;

    //hori lines
    var x1 = 0,
        x2 = width;
    for (var i = 0; i < row; i++) {
        g.append('line')
            .attr('x1', x1)
            .attr('x2', x2)
            .attr('y1', i * height / row)
            .attr('y2', i * height / row)
            .attr('stroke', '#CCC')
            .attr('stroke-width', 0.5)
    }
    //vertical lines
    var y1 = 0,
        y2 = height;
    for (var i = 0; i < col; i++) {
        g.append('line')
            .attr('x1', i * width / col)
            .attr('x2', i * width / col)
            .attr('y1', y1)
            .attr('y2', y2)
            .attr('stroke', '#CCC')
            .attr('stroke-width', 0.5)
    }
}

var drawPath = function(g, data) {
    var conf = Template.mainview.configure.img;

    var pathView = d3.dotPath()
        .width(conf.width)
        .height(conf.height);

    pathView(g, data)
}

var str2sec = function(str) {
    var inputFormat = 'YYYY-M-DD HH:mm:ss'
    var parse = moment(str, inputFormat);
    return parse.hour() * 3600 + parse.minute() * 60 + parse.second();
}
function secondsToTime(secs)
{
    var hour = Math.floor(secs / (60 * 60));
   
    var divisor_for_minutes = secs % (60 * 60);
    var minute = Math.floor(divisor_for_minutes / 60);
 
    var divisor_for_seconds = divisor_for_minutes % 60;
    var second = Math.ceil(divisor_for_seconds);
   
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    return '' + hour + ':' + minute + ':' + second;
}

Template.mainview.helpers({
    'selectDay': function() {
        return [{
            day: 0,
        }, {
            day: 1,
        }, {
            day: 2
        }];
    }
})

var checkMdsFilter = function(i) {
    var mdsFilter = Session.get('mdsFilterVisitors');
    var idx = mdsFilter.indexOf(i);
    if (idx == -1) {
        mdsFilter.push(i);
    } else {
        mdsFilter.splice(idx, 1);
    }
    Session.set('mdsFilterVisitors', mdsFilter)
}

Template.mainview.events({
    'click #one_day_visitor': function(event) {
        checkMdsFilter(1)
    },
    'click #two_day_visitor': function(event) {
        checkMdsFilter(2)
    },
    'click #three_day_visitor': function(event) {
        checkMdsFilter(3)
    },
    'change #dateSelector': function(event) {
        var val = $(event.target).val();
        var date=+val.split(' ')[1];
        Session.set('date', date);
    },
    'change #inputId': function(event) {
        var val = $(event.target).val();
        Session.set('highlightNode', val);
    },

});

Meteor.startup(function() {
    Session.set('mdsFilterVisitors', [])
    Session.set('date', 0)
    Session.set('selectedTime', undefined)
    Session.set('mouseTrack', undefined)
    Session.set('mouseTS', undefined)
})
