// print(moment(one.Timestamp).format('dddd'))

db.mc1.ensureIndex({id:1, 'Timestamp':1})
var days=['2014-6-06 08:00:00', '2014-6-07 08:00:00', '2014-6-08 08:00:00', '2014-6-09 08:00:00']
var totalIdArr=[]

var distance=function(arr1, arr2){
    var len=arr2.length;
    var sum=0;
    for(var i=0;i<len;i++){
        var a=arr1[i], b=arr2[i];
        sum+=((a-=b)*a);
    }
    return Math.sqrt(sum);
}

var mds= function(distances, dimensions) {
    dimensions = dimensions || 2;

    // square distances
    var M = numeric.mul(-0.5, numeric.pow(distances, 2));

    // double centre the rows/columns
    function mean(A) { return numeric.div(numeric.add.apply(null, A), A.length); }
    var rowMeans = mean(M),
        colMeans = mean(numeric.transpose(M)),
        totalMean = mean(rowMeans);

    for (var i = 0; i < M.length; ++i) {
        for (var j =0; j < M[0].length; ++j) {
            M[i][j] += totalMean - rowMeans[i] - colMeans[j];
        }
    }

    // take the SVD of the double centred matrix, and return the
    // points from it
    var ret = numeric.svd(M),
        eigenValues = numeric.sqrt(ret.S);
    return ret.U.map(function(row) {
        return numeric.mul(row, eigenValues).splice(0, dimensions);
    });
};


for(var d=0;d<days.length-1;d++){
    print('day: '+days[d])
    var ids=db.mc1.distinct('id', {'Timestamp':{$lt:days[d+1], $gt:days[d]}});

    for(var i=0;i<ids.length;i++){
        if(i%100==0)print(i)
        var id=ids[i];
        var seq=db.mc1.find({id:id, 'Timestamp':{$lt:days[d+1], $gt:days[d]}}).toArray();
        var idArr=[]
        for(var j=0;j<10000;j++) idArr.push(0);
        for(var j=0;j<seq.length;j++){
            var step=seq[j];
            idArr[step.X*100+step.Y]=1;
        }
        totalIdArr.push(idArr)
    }

    print('cal matrix')
    print('number of people '+totalIdArr.length)
    var matrix=[]
    for(var i=0;i<totalIdArr.length;i++){
        if(i%50==0)print(i)
        var temp=[];
        for(var j=i;j<totalIdArr.length;j++){
            var id1=totalIdArr[i]
            var id2=totalIdArr[j]
            temp.push(distance(id1, id2))
        }
        matrix.push(temp);
    }

    db.similarityMatrix.insert({
        day:days[d],
        m:matrix
    })
    print('cal mds')
    db.mdsPoints.insert({
        day:days[d],
        points:mds(matrix)
    })
}
