var days = ['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']
print('get data')
var id1 = db.mc1.distinct('id', {
    'Timestamp': {
        $lt: days[1]
    }
})
var id2 = db.mc1.distinct('id', {
    'Timestamp': {
        $lt: days[2],
        $gt: days[1]
    }
})
var id3 = db.mc1.distinct('id', {
    'Timestamp': {
        $gt: days[2]
    }
})

print('cal relations')
var same12 = _.intersection(id1, id2);
var same13 = _.intersection(id1, id3);
var same23 = _.intersection(id3, id2);
var threeDay= _.intersection(id1, id2, id3);

var pureSame12=_.difference(same12, threeDay)
var pureSame13=_.difference(same13, threeDay)
var pureSame23=_.difference(same23, threeDay)

var twoDay=_.union(pureSame23, pureSame13, pureSame12);

var all=_.union(id1, id2, id3);
var intersections=_.union(twoDay, threeDay);
var oneDay=_.difference(all, intersections)

print('manipulating dbs')
db.mds.ensureIndex({id:1})
db.mds.update({id:{$in:oneDay}}, {$set:{category:1}}, {multi:true})
db.mds.update({id:{$in:twoDay}}, {$set:{category:2}}, {multi:true})
db.mds.update({id:{$in:threeDay}}, {$set:{category:3}}, {multi:true})

print('ends')